'use strict';

describe('bankApp.version module', function() {
  beforeEach(module('bankApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
