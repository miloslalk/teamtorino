'use strict';

angular.module('bankApp.gestioneAccordo', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/gestioneAccordo', {
    templateUrl: 'js/views/gestioneAccordo.html',
    controller: 'gestioneAccordoCtrl'
  });
}])

.controller('gestioneAccordoCtrl', ['$scope', function($scope) {

  // $scope.test = "milos";

}]);