/// <reference path="../../app.js" />

bankApp.factory('accordioService', ['$http', '$q', function($http, $q) {
    
    return {
        getDettaglioAccordo: function() {

            var deffered = $q.defer();

            $http({
                method: 'GET',
                url: 'http://localhost:3000/dettaglioAccordo'
              }).then(function successCallback(data, status, header, config) {
                console.log(data);
                
                  deffered.resolve(data);
                
                }, function errorCallback(data, status, header, config) {

                    deffered.reject(status);   

                });

                return deffered.promise;
            
        }   
    };
  }]
);


// bankApp.factory('accordioService', function($http) {

//     return {
//         getDettaglioAccordio: function() {
//             var _self;

//             $http.get('http://localhost:3000/api/dettaglioAccordo').then(function(response) {
//   
//                 return response;
//                 //   $scope.DettaglioAccordo = response;
               
//             });


            

//         }
//     };


// });