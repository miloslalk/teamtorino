'use strict';

angular.module('myApp.reportEmpresa', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/reportEmpresa', {
    templateUrl: 'reportEmpresa/reportEmpresa.html',
    controller: 'ReportEmpresaCtrl'
  });
}])

.controller('ReportEmpresaCtrl', [function() {

}]);