'use strict';

// Declare app level module which depends on views, and components
var bankApp = angular.module('bankApp', [
  'ngRoute',
  'bankApp.dettaglioAccordo',
  'bankApp.gestioneAccordo',
  'bankApp.listaPertecipantiAccordo',
  // 'bankApp.blocked',
  // 'bankApp.report',
  // 'bankApp.reportEmpresa',
  // 'bankApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/'});
}]);
